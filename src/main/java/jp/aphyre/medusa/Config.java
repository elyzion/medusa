package jp.aphyre.medusa;

import com.sun.net.httpserver.Filter;
import jp.aphyre.medusa.filter.BodyFilter;
import jp.aphyre.medusa.filter.DefaultHeaderFilter;
import jp.aphyre.medusa.filter.ExceptionFilter;
import jp.aphyre.medusa.filter.UrlQueryStringFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * The global configuration object for the system.
 * <p/>
 * Created by Berthold Alheit on 14/04/20.
 */
public class Config {
    /**
     * The interval for the session cleaning thread to run at.
     */
    public static final int CLEAN_INTERVAL = 60;
    /**
     * How long a session is valid for. (10 minutes)
     */
    public static final long SESSION_TTL = 1000 * 60 * 10;

    /**
     * The port the server runs on.
     */
    public static final int PORT = 8080;
    /**
     * The hostname or IP for the server to listen on.
     */
    public static final String HOST = "localhost";

    /**
     * The size of the highscore list.
     */
    public static final int HIGH_SCORE_LIST_SIZE = 15;
    /**
     * The key under which the parsed request body data will be stored in the HttpExchange object.
     */
    public static final String REQUEST_BODY = "requestBody";
    /**
     * The key under which the parsed data part of the request path will be stored in the HttpExchange object.
     */
    public static final String PATH_MATCH_RESULT = "pathParameter";
    /**
     * The key under which the parsed query string data will be stored in the HttpExchange object.
     */
    public static final String QUERY_PARAMETER = "requestParameters";
    /**
     * The matcher base matcher for url path for this application.
     * In a more extensive application we would have a matcher per path.
     */
    public final static String BASE_URL_PATTERN = "^/(\\d+)/(\\w+)[^/]*$";
    /**
     * The read buffer size for reading in the request body.
     */
    public static final int BODY_READ_BUFFER_SIZE = 2048;
    /**
     * The current version of the API. Should actually read this from properties created at build time.
     */
    public static final String VERSION = "0.1";
    /**
     * Application logger.
     */
    public static final Logger LOGGER = Logger.getLogger("application.log");


    /**
     * Return filters. Called once on startup.
     *
     * @return the list of filters for the system.
     */
    public static List<Filter> filters() {
        ArrayList<Filter> filters = new ArrayList<>();
        filters.add(new ExceptionFilter());
        filters.add(new DefaultHeaderFilter());
        filters.add(new UrlQueryStringFilter());
        filters.add(new BodyFilter());
        return filters;
    }
}