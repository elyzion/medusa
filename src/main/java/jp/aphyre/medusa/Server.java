package jp.aphyre.medusa;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import jp.aphyre.medusa.handler.DefaultHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * The main class for our applications. Starts up the server instance.
 * Registers a hook for shutdown.
 *
 * Created by Berthold Alheit on 14/04/19.
 */
public class Server {

    private HttpServer server;

    public void close(int delay) throws Exception {
        server.stop(delay);
    }

    public void start() throws IOException {
        server = HttpServer.create(new InetSocketAddress(Config.HOST, Config.PORT), 64);
        HttpContext context = server.createContext("/", new DefaultHandler());

        context.getFilters().addAll(Config.filters());
        server.setExecutor(Executors.newWorkStealingPool());

        server.start();
        Config.LOGGER.info("Started server on " + Config.HOST + ":" + Config.PORT);
    }


    public static void main(String[] args) {

        final Server server = new Server();
        try {
            server.start();
        } catch (IOException e) {
            Config.LOGGER.warning("A startup error occured.");
            e.printStackTrace();
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    Config.LOGGER.warning("Shutting down server, please wait.");
                    server.close(1);
                } catch (Exception e) {
                    Config.LOGGER.warning("An error occured on shutdown: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }
}
