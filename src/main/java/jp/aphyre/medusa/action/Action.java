package jp.aphyre.medusa.action;

import com.sun.net.httpserver.HttpExchange;

/**
 * Basic interface for actions.
 * <p/>
 * Created by Berthold Alheit on 14/04/20.
 */
public interface Action {
    /**
     * Called to execute an action.
     *
     * @param httpExchange the httpExchange object.
     * @return the result from executing this action.
     */
    public ActionResult execute(HttpExchange httpExchange);

    /**
     * Called to validate action specific request data and to populate
     * the action instance with data.
     *
     * @param httpExchange the httpExchange object.
     * @return true, false if there was a validation error.
     */
    public boolean checkedPopulate(HttpExchange httpExchange);

}
