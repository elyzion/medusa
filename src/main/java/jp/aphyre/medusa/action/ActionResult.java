package jp.aphyre.medusa.action;

import java.util.Optional;

/**
 * Holds the execution result data from an action.
 * <p/>
 * Created by Berthold Alheit on 14/04/21.
 */
public class ActionResult {
    /**
     * The result HTTP code.
     */
    private final Integer code;
    /**
     * The body data to be returned.
     */
    private final Optional<String> result;

    /**
     * Getter for executing result data in String format.
     *
     * @return Optional containing the result string or Empty()
     */
    public Optional<String> getResult() {
        return result;
    }

    /**
     * Getter for execution results code.
     *
     * @return the result code.
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Convenient constructor for ActionResult.
     *
     * @param code   the result code.
     * @param result the result (body) returned by the execution.
     */
    public ActionResult(Integer code, Optional<String> result) {
        this.code = code;
        this.result = result;
    }
}
