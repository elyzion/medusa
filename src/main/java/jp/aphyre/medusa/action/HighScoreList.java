package jp.aphyre.medusa.action;

import com.sun.net.httpserver.HttpExchange;
import jp.aphyre.medusa.helper.ActionHelper;
import jp.aphyre.medusa.level.LevelRepository;
import jp.aphyre.medusa.score.Score;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static jp.aphyre.medusa.Config.PATH_MATCH_RESULT;

/**
 * Returns a high score list in CSV format. An empty list returns an empty string.
 * <p/>
 * Created by Berthold Alheit on 14/04/21.
 */
public class HighScoreList implements Action {

    /**
     * The levelId
     */
    private Integer levelId;

    /**
     * Called to execute an action.
     *
     * @param httpExchange the httpExchange object.
     * @return the result from executing this action.
     */
    @Override
    public ActionResult execute(HttpExchange httpExchange) {
        List<Score> scores = LevelRepository.getInstance().getLevelHighScores(levelId);
        String csv = scores.stream()
                .map(s -> s.getUserId() + "=" + s.getScore())
                .collect(Collectors.joining(","));
        return new ActionResult(HttpURLConnection.HTTP_OK, Optional.of(csv));
    }

    /**
     * Called to validate action specific request data and to populate
     * the action instance with data.
     *
     * @param httpExchange the httpExchange object.
     * @return true, false if there was a validation error.
     */
    @Override
    public boolean checkedPopulate(HttpExchange httpExchange) {
        //LevelId is required, parsed from request path.
        Optional<Integer> thisLevelId = ActionHelper.getPositiveIntFromObjectString(httpExchange.getAttribute(PATH_MATCH_RESULT));
        if (thisLevelId.isPresent()) {
            levelId = thisLevelId.get();
            return true;
        }
        return false;
    }

}
