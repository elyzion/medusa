package jp.aphyre.medusa.action;

import com.sun.net.httpserver.HttpExchange;
import jp.aphyre.medusa.helper.ActionHelper;
import jp.aphyre.medusa.session.SessionManager;

import java.net.HttpURLConnection;
import java.util.Optional;

import static jp.aphyre.medusa.Config.PATH_MATCH_RESULT;

/**
 * Creates a session key that is associated with a user id.
 * <p/>
 * Created by Berthold Alheit on 14/04/20.
 */
public class Login implements Action {

    /**
     * The user id for which to generate a session key.
     */
    private Integer userId;

    /**
     * {@inheritDoc}
     */
    @Override
    public ActionResult execute(HttpExchange httpExchange) {
        try {
            String sessionKey = SessionManager.getInstance().createSession(userId);
            return new ActionResult(HttpURLConnection.HTTP_OK, Optional.of(sessionKey));
        } catch (Exception e) {
            return new ActionResult(HttpURLConnection.HTTP_INTERNAL_ERROR, Optional.empty());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean checkedPopulate(HttpExchange httpExchange) {
        //UserId is required, parsed from request path.
        Optional<Integer> thisUserId = ActionHelper.getPositiveIntFromObjectString(httpExchange.getAttribute(PATH_MATCH_RESULT));
        if (thisUserId.isPresent()) {
            userId = thisUserId.get();
            return true;
        }
        return false;
    }

}
