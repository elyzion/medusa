package jp.aphyre.medusa.action;

import com.sun.net.httpserver.HttpExchange;
import jp.aphyre.medusa.helper.ActionHelper;
import jp.aphyre.medusa.level.LevelRepository;
import jp.aphyre.medusa.session.Session;
import jp.aphyre.medusa.session.SessionManager;

import java.net.HttpURLConnection;
import java.util.Map;
import java.util.Optional;

import static jp.aphyre.medusa.Config.*;

/**
 * Attempts to record the score for a given user id in the high score list for the specified level.
 * <p/>
 * Created by Berthold Alheit on 14/04/20.
 */
public class Score implements Action {

    /**
     * The level id
     */
    private Integer levelId;

    /**
     * The score
     */
    private Integer score;

    /**
     * The session key for this request.
     */
    private String sessionKey;

    /**
     * {@inheritDoc}
     */
    @Override
    public ActionResult execute(HttpExchange httpExchange) {
        Optional<Session> session = SessionManager.getInstance().getSession(sessionKey);

        if (session.isPresent()) {
            LevelRepository.getInstance().addUserScore(levelId, session.get().getUserId(), score);
            return new ActionResult(HttpURLConnection.HTTP_OK, Optional.empty());
        }

        return new ActionResult(HttpURLConnection.HTTP_UNAUTHORIZED, Optional.empty());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean checkedPopulate(HttpExchange httpExchange) {
        Map<String, String> queryParameters = (Map<String, String>) httpExchange.getAttribute(QUERY_PARAMETER);
        //A sessionKey is required, submitted as query string parameter.
        String thisSessionKey = queryParameters.get("sessionKey".toLowerCase());
        if (thisSessionKey == null || thisSessionKey.length() == 0) {
            return false;
        }
        sessionKey = thisSessionKey;

        //The score has to be submitted as the request body.
        Optional<Integer> body = ActionHelper.getPositiveIntFromObjectString(httpExchange.getAttribute(REQUEST_BODY));
        if (!body.isPresent()) {
            return false;
        }
        score = body.get();

        //LevelId is required, parsed from request path.
        Optional<Integer> thisLevelId = ActionHelper.getPositiveIntFromObjectString(httpExchange.getAttribute(PATH_MATCH_RESULT));
        if (!thisLevelId.isPresent()) {
            return false;
        }
        levelId = thisLevelId.get();

        return true;
    }

}
