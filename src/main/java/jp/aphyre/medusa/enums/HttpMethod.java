package jp.aphyre.medusa.enums;

/**
 * HTTP/1.1 methods.
 * <p/>
 * Created by Berthold Alheit on 14/04/21.
 */
public enum HttpMethod {
    POST, GET, HEAD, DELETE, PUT, TRACE, CONNECT, OPTIONS
}
