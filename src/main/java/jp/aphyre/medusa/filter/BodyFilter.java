package jp.aphyre.medusa.filter;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;
import jp.aphyre.medusa.Config;
import jp.aphyre.medusa.helper.HttpHelper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLDecoder;

/**
 * Filter for reading the request body.
 * <p/>
 * Created by Berthold Alheit on 14/04/19.
 */
public class BodyFilter extends Filter {
    /**
     * Reads the request body and stores in the the HttpExchange instance
     * attributes.
     *
     * @param httpExchange the httpExchange instance.
     * @param chain        the chain.
     * @throws IOException
     */
    @Override
    public void doFilter(HttpExchange httpExchange, Chain chain) throws IOException {
        if (HttpHelper.isPost(httpExchange)) {

            //It would be better to look at content-type and length before processing if this application
            //goes to production.
            InputStream inputStream = httpExchange.getRequestBody();

            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(Config.BODY_READ_BUFFER_SIZE)) {
                byte[] buffer = new byte[Config.BODY_READ_BUFFER_SIZE];
                while (true) {
                    int length = inputStream.read(buffer);
                    if (length == -1)
                        break;
                    outputStream.write(buffer, 0, length);
                }

                if (outputStream.size() > 0) {
                    String body = URLDecoder.decode(outputStream.toString(), "UTF-8");
                    httpExchange.setAttribute(Config.REQUEST_BODY, body);
                }

            } catch (IOException e) {
                Config.LOGGER.warning("An error occurred during IO processing: " + e.getMessage());
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, -1);
                httpExchange.getResponseBody().close();
                return;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
            }
        }

        chain.doFilter(httpExchange);
    }

    @Override
    public String description() {
        return "Decodes the request body for post requests. " +
                "Does not decode it as a set of parameters as this " +
                "is not required in the scope of this scope of this system.";
    }
}
