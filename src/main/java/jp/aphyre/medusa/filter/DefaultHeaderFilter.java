package jp.aphyre.medusa.filter;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import jp.aphyre.medusa.Config;

import java.io.IOException;

/**
 * Adds some default headers.
 * <p/>
 * Created by Berthold Alheit on 14/04/19.
 */
public class DefaultHeaderFilter extends Filter {
    /**
     * Default headers for the service and for correctness.
     *
     * @param httpExchange the httpExchange
     * @param chain the chain
     * @throws java.io.IOException
     */
    @Override
    public void doFilter(HttpExchange httpExchange, Chain chain) throws IOException {
        Headers headers = httpExchange.getResponseHeaders();
        headers.set("Content-Type", "text/plain");
        headers.set("x-api-version", Config.VERSION);
        chain.doFilter(httpExchange);
    }

    @Override
    public String description() {
        return "Appends default headers";
    }
}
