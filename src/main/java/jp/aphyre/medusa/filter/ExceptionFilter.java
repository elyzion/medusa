package jp.aphyre.medusa.filter;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;
import jp.aphyre.medusa.Config;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Filter to catch any unhandled exceptions from the filter chain.
 * <p/>
 * Created by Berthold Alheit on 14/04/19.
 */
public class ExceptionFilter extends Filter {
    /**
     * Catches any unprocessed exception. Prints out simple message to console.
     *
     * @param httpExchange the httpExchange instance.
     * @param chain        the chain.
     * @throws IOException
     */
    @Override
    public void doFilter(HttpExchange httpExchange, Chain chain) throws IOException {
        try {
            chain.doFilter(httpExchange);
        } catch (Exception e) {
            //Pokemon exception to tie up any lose ends.
            Config.LOGGER.warning("An unhandled exception occurred: " + e.getMessage());

            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, -1);
            httpExchange.getResponseBody().close();
        }
    }

    @Override
    public String description() {
        return "Catches and processes unhandled exceptions.";
    }
}
