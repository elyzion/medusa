package jp.aphyre.medusa.filter;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;
import jp.aphyre.medusa.Config;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Filter for parsing url query string into a parameter map.
 * <p/>
 * Created by Berthold Alheit on 14/04/19.
 */
public class UrlQueryStringFilter extends Filter {
    /**
     * Parses the query string into a parameter map.
     *
     * @param httpExchange the httpExchange.
     * @param chain        the filter chain.
     * @throws IOException
     */
    @Override
    public void doFilter(HttpExchange httpExchange, Chain chain) throws IOException {
        String query = httpExchange.getRequestURI().getQuery();

        if (query != null && !query.isEmpty()) {
            Map<String, String> queryMap = new LinkedHashMap<>();
            String[] queryPairs = query.split("&");
            if (queryPairs.length > 0) {
                for (String pair : queryPairs) {
                    int index = pair.indexOf("=");
                    if (index != -1)
                        queryMap.put(
                                URLDecoder.decode(pair.substring(0, index), "UTF-8").toLowerCase(),
                                URLDecoder.decode(pair.substring(index + 1), "UTF-8"));
                    else
                        queryMap.put(URLDecoder.decode(pair, "UTF-8").toLowerCase(), null);
                }
                httpExchange.setAttribute(Config.QUERY_PARAMETER, queryMap);
            }
        }

        chain.doFilter(httpExchange);
    }

    @Override
    public String description() {
        return "Breaks down and prepares query string for consumption";
    }
}
