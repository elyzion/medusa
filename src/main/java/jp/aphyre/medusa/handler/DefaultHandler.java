package jp.aphyre.medusa.handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import jp.aphyre.medusa.Config;
import jp.aphyre.medusa.action.*;
import jp.aphyre.medusa.helper.HttpHelper;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is the default handler for our context.
 * It handles the main logic of our program.
 * <p>
 * Created by Berthold Alheit on 14/04/20.
 */
public class DefaultHandler implements HttpHandler {

    /**
     * Invokes the target action and writes the response.
     *
     * @param httpExchange
     * @throws IOException
     */
    public void handle(HttpExchange httpExchange) throws IOException {
        if (!HttpHelper.isSupportedMethod(httpExchange)) {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, -1);
            httpExchange.getResponseBody().close();
        }

        //Look up the action.
        Optional<Action> action = getAction(httpExchange);

        //If there is no matching action we return an HTTP_NOT_FOUND.
        if (!action.isPresent()) {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, -1);
            httpExchange.getResponseBody().close();
            return;
        }

        //Populate the action with data from our httpExchange. Return HTTP_BAD_REQUEST on validation failure.
        if (!action.get().checkedPopulate(httpExchange)) {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, -1);
            httpExchange.getResponseBody().close();
            return;
        }

        //Execute the business logic for the request.
        ActionResult result = action.get().execute(httpExchange);

        if (result.getResult().isPresent()) {
            httpExchange.sendResponseHeaders(result.getCode(), result.getResult().get().length());
            try (OutputStream os = httpExchange.getResponseBody()) {
                os.write(result.getResult().get().getBytes());
            } catch (IOException e) {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, -1);
                httpExchange.getResponseBody().close();
            }
        } else {
            httpExchange.sendResponseHeaders(result.getCode(), 0);
            httpExchange.getResponseBody().close();
        }
    }


    /**
     * Looks up the action for this httpExchange's request path.
     * The path is already broken up into it's constituent parts.
     * This is sufficient for our use case.
     * <p>
     * Using a full regex for matching the action and then extracting
     * the action and other parameter from it would be a better approach
     * as it would allow for better modifiability, the first commit of this
     * project made use of such an approach, but it was discarded in favour
     * of the current approach. (Sacrificing flexibility for simplicity.)
     *
     * //TODO: Revert to the Action repository mapping action paths to action objects.
     *
     * @param httpExchange
     * @return
     */
    private Optional<Action> getAction(HttpExchange httpExchange) {
        String path = httpExchange.getRequestURI().getPath();
        Matcher matcher = Pattern.compile(Config.BASE_URL_PATTERN).matcher(path);

        if (matcher.find()) {
            httpExchange.setAttribute(Config.PATH_MATCH_RESULT, matcher.group(1));
            String actionName = matcher.group(2);
            if (HttpHelper.isGet(httpExchange)) {
                switch (actionName) {
                    case "login":
                        return Optional.of(new Login());
                    case "highscorelist":
                        return Optional.of(new HighScoreList());
                    default:
                        break;
                }
            } else if (HttpHelper.isPost(httpExchange)) {
                if (actionName.equals("score")) {
                    return Optional.of(new Score());
                }
            }
        }
        return Optional.empty();
    }

}
