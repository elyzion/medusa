package jp.aphyre.medusa.helper;

import java.util.Optional;

/**
 * A pure utility class for the casting of a string to a positive integer.
 * <p/>
 * Created by Berthold Alheit on 14/04/23.
 */
public class ActionHelper {
    /**
     * Tries to cast value to a an integer and perform validation for a positive integer.
     * For our purposes we can handle an invalid number and a lacking number as the same case.
     * (Usually we would want to differentiate between a lacking parameter and and incorrect parameter.)
     *
     * @param value the value to convert and validate.
     * @return The converted value wrapped in an optional instance.
     */
    public static Optional<Integer> getPositiveIntFromObjectString(Object value) {
        if (value == null) {
            return Optional.empty();
        }
        try {
            Integer intValue = Integer.parseInt(value.toString());
            if (intValue <= 0) {
                return Optional.empty();
            }
            return Optional.of(intValue);
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }
}
