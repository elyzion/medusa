package jp.aphyre.medusa.helper;

import com.sun.net.httpserver.HttpExchange;
import jp.aphyre.medusa.enums.HttpMethod;

/**
 * Contains helper methods for working with HTTP requests.
 *
 * Created by Berthold Alheit on 14/04/27.
 */
public class HttpHelper {

    /**
     * Returns true if a request is a POST request.
     *
     * @param httpExchange the current httpexchange
     * @return true or false
     */
    public static boolean isPost(HttpExchange httpExchange) {
        return httpExchange.getRequestMethod().equalsIgnoreCase(HttpMethod.POST.toString());
    }

    /**
     * Returns true if a request is a GET request.
     *
     * @param httpExchange the current httpexchange
     * @return true or false
     */
    public static boolean isGet(HttpExchange httpExchange) {
        return httpExchange.getRequestMethod().equalsIgnoreCase(HttpMethod.GET.toString());
    }

    /**
     * Returns true if a request is a GET or POST request.
     *
     * @param httpExchange the current httpexchange
     * @return true or false
     */
    public static boolean isSupportedMethod(HttpExchange httpExchange) {
        return isPost(httpExchange) || isGet(httpExchange);
    }
}
