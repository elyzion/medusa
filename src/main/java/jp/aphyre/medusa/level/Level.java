package jp.aphyre.medusa.level;

import jp.aphyre.medusa.Config;
import jp.aphyre.medusa.score.Score;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * This represents a single list of high scores.
 * <p/>
 * Created by Berthold Alheit on 14/04/20.
 */
public class Level {

    /**
     * Stores scores.
     */
    private ConcurrentSkipListSet<Score> scores = new ConcurrentSkipListSet<>(Comparator.<Score>reverseOrder());
    /**
     * Stores a read only high score list.
     */
    private List<Score> highScores = new ArrayList<>();

    /**
     * Locking for concurrent read/write operations.
     */
    private final Semaphore lock = new Semaphore(1);
    /**
     * Atomic list size counter, LongAdder *might* be better.
     */
    private final AtomicInteger counter = new AtomicInteger();

    /**
     * Returns a set comprised of the top 15 scores, pre-sorted in descending order.
     *
     * @return list of scores in descending order.
     */
    public List<Score> getHighScores() {
        return highScores;
    }

    /**
     * Adds the given userId and score to the score list.
     * Returns false if the score could not be added.
     * <p/>
     * Scores are added on the following conditions:
     * 1) The list has less or equal to than 15 entries.
     * 3) The score is greater than the least score in the list, or the list is empty.
     * 4) The userId already has a score in the list and the existing score is less than the new score.
     * <p/>
     * Score greatness is calculated by in the following order:
     * The literal score (desc), the time of achievement (desc) and the user id (asc).
     * <p/>
     * Note: we could turn this into an asynchronous task that just schedules
     * the write and returns. If we do this, the client will however not be
     * informed if the write fails, which is not desirable for a backend API.
     *
     * @param userId the user id
     * @param score  the score
     * @return
     */
    public boolean addScore(Integer userId, Integer score) {
        Score newScore = new Score(userId, score);

        Score min = counter.get() > 0 ? scores.last() : null;
        if (min != null && min.compareTo(newScore) > 0 && counter.get() == Config.HIGH_SCORE_LIST_SIZE) {
            //This is the most common execution path.
            //The submitted score is less than the lowest score and the list already has (at least) 15 elements.
            return false;
        } else if (scores.contains(newScore)) {
            //This represent a Score object where the following holds: Same score, same userId, same insertion time.
            //In this case we can assume of double submission in a relatively small time frame and we can safely return
            //false.
            return false;
        }

        //Problem: scores.add calls adds a user,
        //but before the call to scores.add,　we also do getUserScore in another thread.
        //If the added score in both cases is for the same user, with different scores, we can
        //end up having the same user on the score list twice.
        //Thus, we either have some weird trimming logic, or we can just lock this entire execution block.
        try {
            lock.acquireUninterruptibly();
            Optional<Score> userScore = getUserScore(userId);
            if (userScore.isPresent() && userScore.get().compareTo(newScore) > 0) {
                //The user score is already present in the list with a better ranking than the submitted score.
                return false;
            }

            //Add the new score.
            //This can overlap with update high score calls as the list size is limited to 15,
            //so it doesn't matter if we have 16 elements until we call pollLast()
            if (scores.add(newScore)) {
                if (userScore.isPresent()) {
                    //If we are replacing an existing user, then we are replacing the old one
                    scores.remove(userScore.get());
                } else if (counter.get() < Config.HIGH_SCORE_LIST_SIZE) {
                    //else if we still have less than 15 elements, we increment.
                    counter.incrementAndGet();
                } else {
                    //otherwise we remove the last element from the list.
                    scores.pollLast();
                }
                updateHighScores();
                return true;
            }
            return false;
        } finally {
            //Releasing in a try/finally to ensure release no matter what.
            lock.release();
        }
    }

    /**
     * Updates the high score list.
     * The high score list if size limited to 15 scores in descending order.
     */
    public void updateHighScores() {
        highScores = Collections.unmodifiableList(scores
                .stream()
                .sequential()
                .limit(Config.HIGH_SCORE_LIST_SIZE)
                .collect(Collectors.toList()));
    }


    /**
     * Returns the score object for a given score.
     * <p/>
     * NOTE: This is used for testing purposes and may be
     * removed in the future.
     *
     * @param score the score to retrieve.
     * @return An Optional containing the score.
     */
    public Optional<Score> getScore(Integer score) {
        for (Score scoreTarget : scores) {
            if (scoreTarget.getScore().equals(score)) {
                return Optional.of(scoreTarget);
            }
        }
        return Optional.empty();
    }

    /**
     * Return a score object if there exists a score for this userId,
     * otherwise returns null.
     *
     * @param userId the userId to retrieve the score for
     * @return An Optional containing the score.
     */
    public Optional<Score> getUserScore(Integer userId) {
        for (Score score : scores) {
            if (score.getUserId().equals(userId)) {
                return Optional.of(score);
            }
        }
        return Optional.empty();
    }
}
