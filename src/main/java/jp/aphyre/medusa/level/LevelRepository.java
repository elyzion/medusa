package jp.aphyre.medusa.level;

import jp.aphyre.medusa.score.Score;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Repository for level objects.
 * <p/>
 * Created by Berthold Alheit on 14/04/20.
 */
public class LevelRepository {

    /**
     * The map containing all the levels.
     */
    private final ConcurrentHashMap<Integer, Level> levels = new ConcurrentHashMap<>();

    /**
     * Private constructor.
     */
    private LevelRepository() {
    }

    /**
     * Singleton access method.
     *
     * @return
     */
    public static LevelRepository getInstance() {
        return Holder.INSTANCE;
    }

    /**
     * Internal class used for holder pattern.
     */
    private static class Holder {
        private static final LevelRepository INSTANCE = new LevelRepository();
    }

    /**
     * Retrieves a list of highscores for a given level, if there is no such
     * level, then it returns an empty list.
     *
     * @param levelId the levelId.
     * @return list of sorted scores, or empty list.
     */
    public List<Score> getLevelHighScores(Integer levelId) {
        if (!levels.containsKey(levelId)) {
            return new ArrayList<>();
        }
        return levels.get(levelId).getHighScores();
    }

    /**
     * Attempts to add a score for a user to a level. If the level does not exist,
     * it instantiates the level. It then attempts to add the score and userId to
     * the level. If successful, return true, else false.
     *
     * @param levelId the levelId.
     * @param userId  the userId.
     * @param score   the score.
     * @return if successful, true, else false.
     */
    public boolean addUserScore(Integer levelId, Integer userId, Integer score) {
        if (levels.containsKey(levelId)) {
            return levels.get(levelId).addScore(userId, score);
        } else {
            Level newLevel = new Level();
            Level existingValue = levels.putIfAbsent(levelId, newLevel);
            if (existingValue != null) {
                return existingValue.addScore(userId, score);
            } else {
                return newLevel.addScore(userId, score);
            }
        }
    }

    /**
     * Reset the collection by removing all keys.
     */
    public void reset() {
        levels.clear();
    }
}
