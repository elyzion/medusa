package jp.aphyre.medusa.score;

/**
 * Represents a single score comprised of a score value and the corresponding userId.
 * <p/>
 * Created by Berthold Alheit on 14/04/20.
 */
public class Score implements Comparable<Score> {

    /**
     * The user associated with this score.
     * UserId is ignored for comparison purposes.
     */
    private final Integer userId;
    /**
     * The score.
     */
    private Integer score;
    /**
     * The creation time offset.
     * This does not get exposed externally, so we use a primitive.
     */
    private Long timeOffset;

    /**
     * Utility constructor.
     *
     * @param userId the userId
     * @param score  the score
     */
    public Score(Integer userId, Integer score) {
        this.userId = userId;
        this.score = score;
        this.timeOffset = System.nanoTime();
    }

    /**
     * Allows for specifying score achievement time offset as well.
     * This should not be used outside of unit testing.
     *
     * @param userId
     * @param score
     * @param timeOffset
     */
    public Score(Integer userId, Integer score, Long timeOffset) {
        this.userId = userId;
        this.score = score;
        this.timeOffset = timeOffset;
    }


    /**
     * Overrided compareTo without error checking.
     *
     * @param otherScore the other score
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     */
    @Override
    public int compareTo(Score otherScore) {
        if (score.compareTo(otherScore.getScore()) == 0) {
            if (timeOffset.compareTo(otherScore.getTimeOffset()) == 0) {
                return userId.compareTo(otherScore.getUserId());
            } else {
                return timeOffset.compareTo(otherScore.getTimeOffset());
            }
        } else {
            return score.compareTo(otherScore.getScore());
        }
    }

    /**
     * Equality method based on score comparison.
     *
     * @param o the score to compare to
     * @return true if equal, else false.
     */
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null || o.getClass() != this.getClass()) {
            return false;
        }

        Score otherScore = (Score) o;
        return score.equals(otherScore.score) && userId.equals(otherScore.userId)
                && timeOffset.equals(otherScore.getTimeOffset());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + score;
        result = prime * result + userId;
        result = prime * result + (int) ((timeOffset >> 32) ^ timeOffset);
        return result;
    }

    /**
     * Getter for userId.
     *
     * @return the userId.
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * Getter for score.
     *
     * @return the score.
     */
    public Integer getScore() {
        return score;
    }

    /**
     * Getter for timeOffset.
     *
     * @return the timeOffset.
     */
    public Long getTimeOffset() {
        return timeOffset;
    }
}
