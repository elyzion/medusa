package jp.aphyre.medusa.session;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Generating very simple reasonably secure session keys.
 * A more secure approach would involve creating a message
 * digest and then encoding it to a set of alphanumeric characters.
 *
 * The current implementation should be sufficient for our needs.
 *
 * Created by Berthold Alheit on 14/04/27.
 */
public class Generator {

    private final SecureRandom generator;

    /**
     * Private constructor. Initializes cleanup process.
     */
    private Generator() {
        SecureRandom deferedGenerator;
        try {
            deferedGenerator = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            deferedGenerator = new SecureRandom();
        }
        generator = deferedGenerator;
    }

    /**
     * Singleton access method.
     *
     * @return the SessionManager instance.
     */
    public static Generator getInstance() {
        return Holder.INSTANCE;
    }

    /**
     * Internal static class for holder pattern.
     */
    private static class Holder {
        private static final Generator INSTANCE = new Generator();
    }

    /**
     * Very easy random generator. This is a public domain solution for
     * generating reasonable random session id that I have been using since 2012.
     *
     * Credit goes to : http://stackoverflow.com/posts/41156/revisions
     *
     * @return a reasonably random alphan.
     */
    public String generateSession() throws NoSuchAlgorithmException {
        return new BigInteger(128, generator).toString(32);
    }
}
