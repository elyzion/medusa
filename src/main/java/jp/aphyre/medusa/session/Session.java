package jp.aphyre.medusa.session;

/**
 * A simple session object comprised of a userId and creation time.
 * <p/>
 * Created by Berthold Alheit on 14/04/19.
 */
public class Session {
    /**
     * The userId.
     */
    private Integer userId;

    /**
     * The creation time in milliseconds.
     */
    private Long creationTime;

    /**
     * Constructor.
     *
     * @param userId       the userId.
     * @param creationTime the creation time in milliseconds.
     */
    public Session(Integer userId, Long creationTime) {
        this.userId = userId;
        this.creationTime = creationTime;
    }

    /**
     * Getter for userId.
     *
     * @return the userId.
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * Setter for userId.
     *
     * @param userId the userId.
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * Getter for creation time.
     *
     * @return the creation time.
     */
    public Long getCreationTime() {
        return creationTime;
    }

    /**
     * Setter for creation time.
     *
     * @param creationTime the creation time.
     */
    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

}
