package jp.aphyre.medusa.session;

import jp.aphyre.medusa.Config;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * The session manager maintains a list of session objects. It exposes several
 * session operations.
 * <p/>
 * Created by Berthold Alheit on 14/04/19.
 */
public class SessionManager {
    /**
     * Single threaded executor used for cleaning up expired sessions.
     */
    private final ScheduledExecutorService cleaningExecutor = Executors.newSingleThreadScheduledExecutor();
    /**
     * Session key and session index.
     */
    private final ConcurrentHashMap<String, Session> sessionStore = new ConcurrentHashMap<>();

    /**
     * Session cleanup Runnable that loops over all entries in the session store in parrallel
     * and collect expired entries. Expired entries are then deleted.
     */
    private final Runnable cleaner = () -> {
        long currentTime = Calendar.getInstance().getTimeInMillis();

        ArrayList<String> expiredKeys = sessionStore.entrySet().parallelStream()
                .filter(entry -> currentTime - entry.getValue().getCreationTime() > Config.SESSION_TTL)
                .map(Map.Entry::getKey)
                .collect(Collectors.toCollection(ArrayList::new));

        Config.LOGGER.info(Calendar.getInstance().getTime().toString() + " Cleaning up " + expiredKeys.size() + " expired sessions");
        expiredKeys.forEach(sessionStore::remove);

    };

    /**
     * Private constructor. Initializes cleanup process.
     */
    private SessionManager() {
        cleaningExecutor.scheduleAtFixedRate(cleaner, Config.CLEAN_INTERVAL, Config.CLEAN_INTERVAL, TimeUnit.SECONDS);
    }

    /**
     * Singleton access method.
     *
     * @return the SessionManager instance.
     */
    public static SessionManager getInstance() {
        return Holder.INSTANCE;
    }

    /**
     * Internal static class for holder pattern.
     */
    private static class Holder {
        private static final SessionManager INSTANCE = new SessionManager();
    }

    /**
     * Creates a new session associated with the provided userId.
     *
     * @param userId the userId.
     * @return the sessionKey.
     * @throws Exception if sessionKey creation failed.
     */
    public String createSession(Integer userId) throws Exception {
        String sessionKey = Generator.getInstance().generateSession();
        int count = 0;
        Session session = new Session(userId, Calendar.getInstance().getTimeInMillis());
        Session oldValue = sessionStore.putIfAbsent(sessionKey, session);

        //Insertion was successful
        if (oldValue == null)
            return sessionKey;

        //The given sessionKey already existing,
        //try another sessionKey. Maximum of 10 tries.
        while (oldValue != null) {
            sessionKey = Generator.getInstance().generateSession();
            oldValue = sessionStore.putIfAbsent(sessionKey, session);
            count++;
            if (count > 10) {
                throw new Exception("Could not create session");
            }

        }
        return sessionKey;
    }

    /**
     * Return true if the given session is valid, false otherwise.
     * This function has a side-effect; invalid session are removed
     * from the session store.
     *
     * @param sessionKey the sessionKey.
     * @return true if valid, else false.
     */
    public boolean isValidSession(String sessionKey) {
        Session session = sessionStore.get(sessionKey);
        return isValidSession(sessionKey, session);
    }

    /**
     * Return the session object associated with the given sessionKey. If no such
     * object exists, null is returned. This function has side-effects; If the
     * session has expired it will be removed from the session store and null will be returned.
     *
     * @param sessionKey the sessionKey.
     * @return the session object or null.
     */
    public Optional<Session> getSession(String sessionKey) {
        Session session = sessionStore.get(sessionKey);
        if (!isValidSession(sessionKey, session)) {
            return Optional.empty();
        } else {
            return Optional.of(session);
        }
    }

    /**
     * Checks a session for validity.
     *
     * @param session
     * @return
     */
    private boolean isValidSession(String sessionKey, Session session) {
        if (session == null)
            return false;

        long currentTime = Calendar.getInstance().getTimeInMillis();
        if (currentTime - session.getCreationTime() > Config.SESSION_TTL) {
            sessionStore.remove(sessionKey);
            return false;
        }
        return true;
    }

    /**
     * Invalidates a session without removing it.
     *
     * @param sessionKey
     */
    public void invalidateSession(String sessionKey) {
        Session session = sessionStore.get(sessionKey);
        if (session != null) {
            session.setCreationTime(1L);
        }
    }

    /**
     * Removed the specified sessionKey from the session store.
     *
     * @param sessionKey
     */
    public void removeSession(String sessionKey) {
        sessionStore.remove(sessionKey);
    }

    /**
     * Reset the collection by removing all keys.
     */
    public void reset() {
        sessionStore.clear();
    }
}
