package jp.aphyre.medusa.test.benchmark;

import jp.aphyre.medusa.level.LevelRepository;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This is a very naive little benchmark for testing random reads and writes
 * to the level repository.
 * <p/>
 * Created by Berthold Alheit on 14/04/20.
 */
public class LevelBenchMark {

    public final static int COUNT = 100000;

    @Test
    public void randomWriteBench() throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(512);
        List<Callable<Boolean>> taskList = new ArrayList<>();

        for (int i = 0; i < COUNT; i++) {
            taskList.add(() -> {
                int level = (int) (Math.random() * 1000000) % 1000;
                int userId = (int) (Math.random() * 1000000) % 10000;
                int score = (int) (Math.random() * 100000000) % 100000;
                return LevelRepository.getInstance().addUserScore(level, userId, score);
            });
        }
        long timeStartFuture = Calendar.getInstance().getTimeInMillis();
        executor.invokeAll(taskList).parallelStream().forEach(future -> {
            try {
                future.get();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });
        long timeEndFuture = Calendar.getInstance().getTimeInMillis();
        long timeNeededFuture = timeEndFuture - timeStartFuture;
        System.out.println("Performed " + COUNT + " write ops in " + timeNeededFuture + " ms");
        executor.shutdown();
    }

    @Test
    public void randomReadBench() throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(512);
        List<Callable<Boolean>> taskList = new ArrayList<>();

        for (int i = 0; i < COUNT; i++) {
            taskList.add(() -> {
                int level = (int) (Math.random() * 1000000) % 1000;
                int userId = (int) (Math.random() * 1000000) % 10000;
                int score = (int) (Math.random() * 100000000) % 100000;
                return LevelRepository.getInstance().addUserScore(level, userId, score);
            });
        }

        executor.invokeAll(taskList).parallelStream().forEach(future -> {
            try {
                future.get();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });


        taskList = new ArrayList<>();
        for (int i = 0; i < COUNT; i++) {
            taskList.add(() -> {
                int level = (int) (Math.random() * 100000) % 1000;
                return LevelRepository.getInstance().getLevelHighScores(level) != null;
            });
        }
        long timeStartFuture = Calendar.getInstance().getTimeInMillis();
        executor.invokeAll(taskList).parallelStream().forEach(future -> {
            try {
                future.get();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });
        long timeEndFuture = Calendar.getInstance().getTimeInMillis();
        long timeNeededFuture = timeEndFuture - timeStartFuture;
        System.out.println("Performed " + COUNT + " read ops in " + timeNeededFuture + " ms");
        executor.shutdown();
    }

}
