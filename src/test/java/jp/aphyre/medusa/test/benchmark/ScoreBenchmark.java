package jp.aphyre.medusa.test.benchmark;

import jp.aphyre.medusa.level.Level;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Berthold Alheit on 14/04/20.
 */
public class ScoreBenchmark {

    private static final int COUNT = 10000;

    @Test
    public void testWriteToTailFullWithNewEntry() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(512);
        List<Callable<Boolean>> taskList = new ArrayList<>();
        Level level = new Level();

        //fill list.
        for (int i = 1; i < 15; i++) {
            int userId = Integer.MAX_VALUE - i;
            int score = Integer.MAX_VALUE - i;
            level.addScore(userId, score);
        }
        //last entry has score 1.
        level.addScore(15, 1);


        for (int i = 0; i < COUNT; i++) {
            taskList.add(() -> {
                int userId = (int) (Math.random() * 100000000) % 1000000;
                int score = COUNT;
                return level.addScore(userId, score);
            });
        }

        long timeStartFuture = Calendar.getInstance().getTimeInMillis();
        executor.invokeAll(taskList).parallelStream().forEach(future -> {
            try {
                future.get();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });
        long timeEndFuture = Calendar.getInstance().getTimeInMillis();
        long timeNeededFuture = timeEndFuture - timeStartFuture;
        System.out.println("Performed " + COUNT + " write ops replacing tail element in " + timeNeededFuture + " ms");
        executor.shutdown();

    }

    @Test
    public void testWriteToHeadFullWithNewEntry() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(512);
        List<Callable<Boolean>> taskList = new ArrayList<>();
        Level level = new Level();

        //fill list.
        for (int i = 1; i < 16; i++) {
            int userId = i;
            int score = i;
            level.addScore(userId, score);
        }


        for (int i = 0; i < COUNT; i++) {
            final int offset = i;
            taskList.add(() -> {
                int userId = (int) (Math.random() * 100000000) % 1000000;
                int score = COUNT + offset;
                return level.addScore(userId, score);
            });
        }

        long timeStartFuture = Calendar.getInstance().getTimeInMillis();
        executor.invokeAll(taskList).parallelStream().forEach(future -> {
            try {
                future.get();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });
        long timeEndFuture = Calendar.getInstance().getTimeInMillis();
        long timeNeededFuture = timeEndFuture - timeStartFuture;
        System.out.println("Performed " + COUNT + " write ops replacing head element in " + timeNeededFuture + " ms");
        executor.shutdown();
    }

    @Test
    public void testAndReplaceExistingUser() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(512);
        List<Callable<Boolean>> taskList = new ArrayList<>();
        Level level = new Level();

        //fill list.
        for (int i = 1; i < 16; i++) {
            int userId = i;
            int score = i;
            level.addScore(userId, score);
        }


        for (int i = 0; i < COUNT; i++) {
            final int offset = i;
            taskList.add(() -> {
                int userId = 1;
                int score = COUNT + offset;
                return level.addScore(userId, score);
            });
        }

        long timeStartFuture = Calendar.getInstance().getTimeInMillis();
        executor.invokeAll(taskList).parallelStream().forEach(future -> {
            try {
                future.get();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });
        long timeEndFuture = Calendar.getInstance().getTimeInMillis();
        long timeNeededFuture = timeEndFuture - timeStartFuture;
        System.out.println("Performed " + COUNT + " write updating user score in " + timeNeededFuture + " ms");
        executor.shutdown();
    }

}
