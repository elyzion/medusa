package jp.aphyre.medusa.test.functional;

import com.jayway.restassured.response.Response;
import jp.aphyre.medusa.Server;
import jp.aphyre.medusa.level.LevelRepository;
import jp.aphyre.medusa.session.SessionManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.with;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: berthold_alheit
 * Date: 14/04/18
 * Time: 15:07
 */
public class ServerFunctionalTest {

    private Server server;

    @Before
    public void createServer() throws IOException {
        server = new Server();
        server.start();
        SessionManager.getInstance().reset();
        LevelRepository.getInstance().reset();
    }

    @After
    public void stop() throws Exception {
        server.close(0);
    }


    @Test
    public void sessionCreation() {
        final Response response = get("/4711/login");
        assertThat(response.getStatusCode(), equalTo(HttpURLConnection.HTTP_OK));
        //Standard UUID match regex dredged from internet.
        assertTrue(response.asString().matches("^([0-9a-z]+)$"));
    }

    @Test
    public void sessionCreationFailsWithLargeUserId() {
        final Response response = get("/" + Long.MAX_VALUE + "/login");
        assertThat(response.getStatusCode(), equalTo(HttpURLConnection.HTTP_BAD_REQUEST));
    }

    @Test
    public void sessionCreationFailsWithNegativeUserId() {
        final Response response = get("/-4711/login");
        assertThat(response.getStatusCode(), equalTo(HttpURLConnection.HTTP_NOT_FOUND));
    }


    @Test
    public void postScore() throws Exception {
        String sessionId = SessionManager.getInstance().createSession(11);
        with().body("1")
                .queryParameters("sessionKey", sessionId)
                .post("/11/score")
                .then()
                .statusCode(HttpURLConnection.HTTP_OK);
    }

    @Test
    public void postScoreFailsWithInvalidSession() {
        with().body("1")
                .queryParameters("sessionKey", "bogus")
                .post("/1/score")
                .then()
                .statusCode(HttpURLConnection.HTTP_UNAUTHORIZED);

        with().body("1")
                .queryParameters("sessionKey", Long.MAX_VALUE)
                .post("/1/score")
                .then()
                .statusCode(HttpURLConnection.HTTP_UNAUTHORIZED);
    }


    @Test
    public void postScoreFailsWithNegativeScore() throws Exception {
        String sessionId = SessionManager.getInstance().createSession(11);
        with().body("-1")
                .queryParameters("sessionKey", sessionId)
                .post("/1/score")
                .then()
                .statusCode(HttpURLConnection.HTTP_BAD_REQUEST);
    }


    @Test
    public void postScoreFailsWithNegativeLevel() throws Exception {
        String sessionId = SessionManager.getInstance().createSession(11);
        with().body("1")
                .queryParameters("sessionKey", sessionId)
                .post("/-1/score")
                .then()
                .statusCode(HttpURLConnection.HTTP_NOT_FOUND);
    }


    @Test
    public void postScoreFailsWithLargeScore() throws Exception {
        String sessionId = SessionManager.getInstance().createSession(11);
        with().body(Long.MAX_VALUE)
                .queryParameters("sessionKey", sessionId)
                .post("/1/score")
                .then()
                .statusCode(HttpURLConnection.HTTP_BAD_REQUEST);
    }


    @Test
    public void postScoreFailsWithLargeLevel() throws Exception {
        String sessionId = SessionManager.getInstance().createSession(11);
        with().body("1")
                .queryParameters("sessionKey", sessionId)
                .post("/" + Long.MAX_VALUE + "/score")
                .then()
                .statusCode(HttpURLConnection.HTTP_BAD_REQUEST);
    }


    @Test
    public void listScore() throws Exception {
        LevelRepository.getInstance().addUserScore(1, 11, 1);
        LevelRepository.getInstance().addUserScore(1, 21, 2);
        LevelRepository.getInstance().addUserScore(1, 31, 3);
        get("/1/highscorelist")
                .then()
                .body(containsString("31=3,21=2,11=1"))
                .and()
                .statusCode(HttpURLConnection.HTTP_OK);

    }

    @Test
    public void listScoreShouldBeEmptyIfNoScores() throws Exception {
        LevelRepository.getInstance().addUserScore(1, 11, 1);
        LevelRepository.getInstance().addUserScore(1, 21, 2);
        LevelRepository.getInstance().addUserScore(1, 31, 3);
        get("/2/highscorelist")
                .then()
                .body(containsString(""))
                .and()
                .statusCode(HttpURLConnection.HTTP_OK);

    }

    @Test
    public void badUrlReturnsNotFound() throws Exception {
        Response response = get("/test/this/url/does/not/exist");
        assertThat(response.getStatusCode(), equalTo(HttpURLConnection.HTTP_NOT_FOUND));

        response = get("/4711/login/url/does/not/exist");
        assertThat(response.getStatusCode(), equalTo(HttpURLConnection.HTTP_NOT_FOUND));

        response = get("/url/does/not/exist/4711/login");
        assertThat(response.getStatusCode(), equalTo(HttpURLConnection.HTTP_NOT_FOUND));

        String sessionId = SessionManager.getInstance().createSession(11);
        with().body("1")
                .queryParameters("sessionKey", sessionId)
                .post("/1/score/very/bad")
                .then()
                .statusCode(HttpURLConnection.HTTP_NOT_FOUND);

        with().body("1")
                .queryParameters("sessionKey", sessionId)
                .post("/somethingelse/bad/very/bad")
                .then()
                .statusCode(HttpURLConnection.HTTP_NOT_FOUND);

        with().body("1")
                .queryParameters("sessionKey", sessionId)
                .post("/somethingelse/bad/very/bad/1/score")
                .then()
                .statusCode(HttpURLConnection.HTTP_NOT_FOUND);

        get("/1/highscorelist/bad/very")
                .then()
                .statusCode(HttpURLConnection.HTTP_NOT_FOUND);

        get("/bad/very/1/highscorelist")
                .then()
                .statusCode(HttpURLConnection.HTTP_NOT_FOUND);
    }


}
