package jp.aphyre.medusa.test.unit;

import jp.aphyre.medusa.helper.ActionHelper;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Berthold Alheit on 14/04/23.
 */
public class ActionHelperTest {

    @Test
    public void nullValue() {
        assertFalse(ActionHelper.getPositiveIntFromObjectString(null).isPresent());
    }

    @Test
    public void longValue() {
        assertFalse(ActionHelper.getPositiveIntFromObjectString("" + Long.MAX_VALUE + "").isPresent());
    }

    @Test
    public void intValue() {
        assertTrue(ActionHelper.getPositiveIntFromObjectString("12121").isPresent());
    }

    @Test
    public void characterValue() {
        assertFalse(ActionHelper.getPositiveIntFromObjectString("This should fail").isPresent());
    }
}
