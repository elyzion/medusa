package jp.aphyre.medusa.test.unit;

import jp.aphyre.medusa.level.LevelRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Berthold Alheit on 14/04/20.
 */
public class LevelRepitoryTest {
    @Before
    public void clearScores() {
        LevelRepository.getInstance().reset();
    }

    @Test
    public void levelReturnsEmptyListWhenEmpty() {
        assertTrue(LevelRepository.getInstance().getLevelHighScores(55).isEmpty());
    }
}
