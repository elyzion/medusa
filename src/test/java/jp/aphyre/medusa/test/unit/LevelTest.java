package jp.aphyre.medusa.test.unit;

import jp.aphyre.medusa.level.Level;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Berthold Alheit on 14/04/20.
 */
public class LevelTest {

    @Test
    public void scoreReturnsNullWhenEmpty() {
        Level repo = new Level();
        assertFalse(repo.getScore(1).isPresent());
    }

    @Test
    public void userReturnsNullWhenEmpty() {
        Level repo = new Level();
        assertFalse(repo.getUserScore(1).isPresent());
    }

    @Test
    public void scoresReturnsEmptyListWhenEmpty() {
        Level repo = new Level();
        assertTrue(repo.getHighScores().isEmpty());
    }

    @Test
    public void scoresAreAddedAndReadCorrectly() {
        Level repo = new Level();
        repo.addScore(1, 1);
        assertTrue(repo.getUserScore(1).isPresent()
                && repo.getUserScore(1).get().getScore().equals(1));
        assertTrue(repo.getScore(1).isPresent()
                && repo.getScore(1).get().getUserId().equals(1)
                && repo.getScore(1).get().getScore().equals(1));
        assertEquals(1, repo.getHighScores().size());
    }

    @Test
    public void aNewerEqualScoreForAUserReplacesOlderScore() {
        Level repo = new Level();
        repo.addScore(1, 1);
        assertTrue(repo.addScore(1, 1));
    }

    @Test
    public void submittingScoreLowerThan15thElementIsIgnored() {
        Level repo = new Level();
        for (int i = 0; i < 15; i++) {
            int score = i + 15;
            assertTrue("Adding score: " + score, repo.addScore(i + 1, score));
        }
        assertFalse(repo.addScore(224, 1));
    }

    @Test
    public void submittingBetterScoreForUserOverwritesOldScore() {
        Level repo = new Level();
        for (int i = 0; i < 15; i++) {
            int score = i + 15;
            assertTrue("Adding score: " + score, repo.addScore(i + 1, score));
        }
        assertEquals(5, repo.getUserScore(5).get().getUserId().intValue());
        int oldScore = repo.getUserScore(5).get().getScore();

        assertTrue(repo.addScore(5, 10000));
        assertEquals(5, repo.getHighScores().get(0).getUserId().intValue());
        assertEquals(10000, repo.getHighScores().get(0).getScore().intValue());
        assertFalse(repo.getScore(oldScore).isPresent());
    }


    @Test
    public void maxHighScoreListSizeIs15Elements() {
        Level repo = new Level();
        for (int i = 0; i < 200; i++) {
            int score = i + 200;
            assertTrue("Adding score: " + score, repo.addScore(i + 1, score));
        }
        assertEquals(15, repo.getHighScores().size());
    }


    @Test
    public void scoresAreOrderedInDescendingOrder() {
        Level repo = new Level();
        repo.addScore(11, 1);
        repo.addScore(21, 2);
        repo.addScore(31, 3);

        assertEquals(3, repo.getHighScores().stream().count());
        assertTrue(repo.getHighScores().get(0).getScore().equals(3));
        assertTrue(repo.getHighScores().get(repo.getHighScores().size() - 1).getScore().equals(1));
    }
}
