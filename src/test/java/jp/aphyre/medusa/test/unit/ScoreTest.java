package jp.aphyre.medusa.test.unit;

import jp.aphyre.medusa.score.Score;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Berthold Alheit on 14/04/20.
 */
public class ScoreTest {

    @Test
    public void scoresWithSameScoreAndDifferentUserIdAreSortedByDescInsertionTime() {
        Score l = new Score(1, 1);
        Score r = new Score(2, 1);
        assertFalse(l.equals(r));
        assertTrue(l.compareTo(r) < 0);
    }

    @Test
    public void ScoreEqualityTest() {
        Score l = new Score(1, 1, 100L);
        Score r = new Score(1, 1, 100L);
        assertTrue(l.equals(r));
        assertTrue(l.compareTo(r) == 0);
    }
}
