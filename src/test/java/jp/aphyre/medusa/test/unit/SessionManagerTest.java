package jp.aphyre.medusa.test.unit;

import jp.aphyre.medusa.session.SessionManager;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Berthold Alheit on 14/04/20.
 */
public class SessionManagerTest {
    @Before
    public void clearScores() {
        SessionManager.getInstance().reset();
    }

    @Test
    public void sessionKeys() throws Exception {
        assertTrue(SessionManager
                .getInstance()
                .createSession(1)
                .matches("^([0-9a-z]+)$"));
    }

    @Test
    public void expiredSessionKeysGetCleanup() throws Exception {
        String sessionId = SessionManager
                .getInstance()
                .createSession(1);
        assertTrue(SessionManager.getInstance().isValidSession(sessionId));
        SessionManager.getInstance().invalidateSession(sessionId);
        assertFalse(SessionManager.getInstance().getSession(sessionId).isPresent());

    }

    @Test
    public void sessionKeyCanBeInvalidated() throws Exception {
        String sessionId = SessionManager
                .getInstance()
                .createSession(1);
        assertTrue(SessionManager.getInstance().isValidSession(sessionId));
        SessionManager.getInstance().invalidateSession(sessionId);
        assertFalse(SessionManager.getInstance().isValidSession(sessionId));
    }

}
